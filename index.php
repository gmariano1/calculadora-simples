<!DOCTYPE html>
<html>
<head>
	<title>Calculadora</title>
</head>
<body>
<form method="post" action="calcula.php">
	Digite o valor 1: <input type="number" name="a">
	Digite o valor 2: <input type="number" name="b">
	<select name="operacao">
		<option disabled selected>Escolha a operação</option>
		<option value="soma">+</option>
		<option value="subtracao">-</option>
		<option value="multiplicacao">*</option>
		<option value="divisao">/</option>
	</select>
	<button>Calcula</button>
</form>
</body>
</html>